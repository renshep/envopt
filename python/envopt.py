import os, sys

def ParseStrings(ainput):
    storage = {}
    if type(ainput) == type([]):
        for item in ainput:
            split = str(item).split("=",1)
            if len(split) > 1:
                storage[split[0]] = split[1]
    return storage

def GetSetting(storage,key,default_value):
    if type(storage) != type({}):
        storage = {}
    return storage.get(key,os.environ.get(key,default_value))

# args = ParseStrings(sys.argv)
