import envopt
import os

# prep tests
os.environ["test_env_var"] = "test_env_var_value"

# test ParseStrings

storage = envopt.ParseStrings([
    "ignore_me",
    "key=val",
    "key2=val=with=multi=equals",
    "blankval=",
    "multiline\nkey=multiline\nval",
    "",
])

print(storage == {'key': 'val', 'key2': 'val=with=multi=equals', 'blankval': '', 'multiline\nkey': 'multiline\nval'})

# test GetSetting
print(envopt.GetSetting(storage,'key','default_value') == 'val')
print(envopt.GetSetting(storage,'key2','default_value') == 'val=with=multi=equals')
print(envopt.GetSetting(storage,'blankval','default_value') == '')
print(envopt.GetSetting(storage,'multiline\nkey','default_value') == 'multiline\nval')
print(envopt.GetSetting(storage,'nonexistant_key','default_value') == 'default_value')
print(envopt.GetSetting(storage,'test_env_var','default_value') == 'test_env_var_value')
