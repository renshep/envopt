package envopt

import (
	"os"
	"testing"
)

func TestParseStrings(t *testing.T) {
	// testing setup
	var input = []string{
		"this string has no equal signs and should be ignored",
		"key=val",
		"key2=val2=with=multiple=equals",
		"key3=multiline\ndata",
		"multiline\nkey=cool",
		"multiline\nkey2=multiline\ndata2",
		"keywithnoval=",
	}
	// test parser
	test_result := ParseStrings(input)
	// test expected results length
	test_result_len := len(test_result)
	if test_result_len != 6 {
		t.Error("Testing Result Len:",test_result_len)
	}
	// test ignored strings
	_, ok := test_result["this string has no equal signs and should be ignored"]
	if ok {
		t.Error("string with no equal not ignored")
	}
	// test keys and values
	if GetSetting(&test_result,"key","default_value") != "val" {
		t.Error("Key error",test_result)
	}
	if GetSetting(&test_result,"key2","default_value") != "val2=with=multiple=equals" {
		t.Error("Key error",test_result)
	}
	if GetSetting(&test_result,"key3","default_value") != "multiline\ndata" {
		t.Error("Key error",test_result)
	}
	if GetSetting(&test_result,"multiline\nkey","default_value") != "cool" {
		t.Error("Key error",test_result)
	}
	if GetSetting(&test_result,"multiline\nkey2","default_value") != "multiline\ndata2" {
		t.Error("Key error",test_result)
	}
	if GetSetting(&test_result,"keywithnoval","default_value") != "" {
		t.Error("Key error",test_result)
	}
}

func TestGetSetting(t *testing.T) {
	// testing setup
	os.Setenv("test_env_var", "test_env_var_value")
	var s = map[string]string{"test_storage_var": "test_storage_var_value"}
	// test default setting
	default_test := GetSetting(&s, "this_key_should_not_exists", "test_default_value")
	if default_test != "test_default_value" {
		t.Error("Default Setting test failed:", default_test)
	}
	// test setting from storage
	storage_test := GetSetting(&s, "test_storage_var", "test_default_value")
	if storage_test != "test_storage_var_value" {
		t.Error("Storage Setting test failed:", storage_test)
	}
	// test setting from environment
	env_test := GetSetting(&s, "test_env_var", "test_default_value")
	if env_test != "test_env_var_value" {
		t.Error("Environment Setting test failed:", env_test)
	}
	// test nil storage
	nil_storage_test := GetSetting(nil, "nil_storage_test", "nil_storage_test_val")
	if nil_storage_test != "nil_storage_test_val" {
		t.Error("Nil storage test failed:", nil_storage_test)
	}
}
